// Build HTML and Gemini site from extended Gemini files
// Copyright (C) 2024  Yann COUTURIER and contributors

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;

pub mod gemini;
pub mod html;

pub fn run(input: std::path::PathBuf, output: std::path::PathBuf) -> Result<()> {
    println!("Building {} -> {}", input.display(), output.display());

    // Our choice is to build Gemini first (follow includes, for example), then
    // build the HTML code once the Gemini code is fully ready.
    match gemini::build(input, output.join("gemini")) {
        Ok(content) => { content },
        Err(error) => { return Err(error.into()); }
    }

    match html::build(output.join("gemini"), output.join("html")) {
        Ok(content) => { content },
        Err(error) => { return Err(error.into()); }
    }

    println!("Built Successfully !");
    Ok(())
}
