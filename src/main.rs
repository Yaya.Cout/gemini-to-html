// Build HTML and Gemini site from extended Gemini files
// Copyright (C) 2024  Yann COUTURIER and contributors

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::fs;
use clap::Parser;
use anyhow::{Context, Result};

pub mod build;


/// Build Gemini website to HTML using template
#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Args {
    /// Input directory
    #[arg(short, long, default_value = "pages")]
    input: std::path::PathBuf,

    /// Output directory
    #[arg(short, long, default_value = "dist")]
    output: std::path::PathBuf,
}

fn check_args(input: std::path::PathBuf, output: std::path::PathBuf) -> Result<()> {
    fs::read_dir(&input)
        .with_context(|| format!("could not read input directory `{}`", input.display()))?;

    if output.is_dir() {
        eprintln!("Warning : output directory already exists");
    }

    fs::create_dir_all(&output)
        .with_context(|| format!("could not create output directory `{}`", output.display()))?;


    // We can use unwrap safely as we already know that both input and output
    // exists (we trust the user to not touch files during processing)
    let canonical_input = fs::canonicalize(&input).unwrap();
    let canonical_output = fs::canonicalize(&output).unwrap();
    if canonical_output.starts_with(canonical_input.clone()) {
        return Err(anyhow::Error::msg(format!("input:  `{}`\noutput: `{}`", canonical_input.display(), canonical_output.display())))
            .with_context(|| "output is a subdirectory of input")?;
    }

    Ok(())
}

fn main() -> Result<()> {
    let args = Args::parse();

    match check_args(args.input.clone(), args.output.clone()) {
        Ok(content) => { content },
        Err(error) => { return Err(error.into()); }
    };

    // Now, we are ready to build !
    match build::run(args.input, args.output) {
        Ok(content) => { content },
        Err(error) => { return Err(error.into()); }
    }

    Ok(())
}