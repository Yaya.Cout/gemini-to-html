// Build HTML and Gemini site from extended Gemini files
// Copyright (C) 2024  Yann COUTURIER and contributors

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::fs;
use anyhow::{Context, Result};

fn build_page(input: std::path::PathBuf, output: std::path::PathBuf) -> Result<()> {
    println!("Building: {} -> {}", input.display(), output.display());
    // TODO: Implement
    fs::copy(input.clone(), output.clone())
        .with_context(|| format!("could copy file `{}` to `{}`", input.display(), output.display()))?;

    Ok(())
}

pub fn build(input: std::path::PathBuf, output: std::path::PathBuf) -> Result<()> {
    fs::create_dir_all(&output)
        .with_context(|| format!("could not create Gemini output directory `{}`", output.display()))?;
    let files = fs::read_dir(&input)
        .with_context(|| format!("could not read directory `{}`", input.display()))?;
    for entry in files {
        let filename = entry.as_ref().unwrap().path().file_name().unwrap().to_str().unwrap().to_string();
        let path = entry.unwrap().path();
        if fs::metadata(path.clone()).with_context(|| format!("could not check if file is a directory `{}`",path.display()))?.is_dir() {
            match build(input.join(filename.clone()), output.join(filename)) {
                Ok(content) => { content },
                Err(error) => { return Err(error.into()); }
            }
        } else {
            match build_page(input.join(filename.clone()), output.join(filename)) {
                Ok(content) => { content },
                Err(error) => { return Err(error.into()); }
            }
        }
    }

    Ok(())
}