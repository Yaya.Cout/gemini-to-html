// Build HTML and Gemini site from extended Gemini files
// Copyright (C) 2024  Yann COUTURIER and contributors

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::fs;
use std::fs::File;
use std::io::{prelude::*, BufReader};

use anyhow::{Context, Result};

#[derive(PartialEq, Copy, Clone)]
enum FormattingMode {
    None,
    Text,
    Header1,
    Header2,
    Header3,
    List,
    Quote,
    Link,
    Preformatted
}

fn formatting_mode_from_line(line: &String) -> FormattingMode {
    if line.starts_with("###") {
        return FormattingMode::Header3;
    } else if line.starts_with("##") {
        return FormattingMode::Header2;
    } else if line.starts_with("#") {
        return FormattingMode::Header1;
    } else if line.starts_with("* ") {
        return FormattingMode::List;
    } else if line.starts_with(">") {
        return FormattingMode::Quote;
    } else if line.starts_with("=>") {
        return FormattingMode::Link;
    } else if line.starts_with("```") {
        return FormattingMode::Preformatted;
    }
    return FormattingMode::Text;
}

fn start_formatting(mode: FormattingMode) -> String {
    match mode {
        FormattingMode::None => { "".to_string() },
        FormattingMode::Text => { "<p>".to_string() },
        FormattingMode::Header1 => { "<h1>".to_string() },
        FormattingMode::Header2 => { "<h2>".to_string() },
        FormattingMode::Header3 => { "<h3>".to_string() },
        FormattingMode::List => { "<ul>".to_string() },
        FormattingMode::Quote => { "<blockquote><p>".to_string() },
        FormattingMode::Link => { "".to_string() },
        FormattingMode::Preformatted => { "<pre>".to_string() }
    }
}

fn end_formatting(mode: FormattingMode) -> String {
    match mode {
        FormattingMode::None => { "".to_string() },
        FormattingMode::Text => { "</p>".to_string() },
        FormattingMode::Header1 => { "</h1>".to_string() },
        FormattingMode::Header2 => { "</h2>".to_string() },
        FormattingMode::Header3 => { "</h3>".to_string() },
        FormattingMode::List => { "</ul>".to_string() },
        FormattingMode::Quote => { "</p></blockquote>".to_string() },
        FormattingMode::Link => { "".to_string() },
        FormattingMode::Preformatted => { "</pre>".to_string() },
    }
}

fn line_without_formatting(line: String, mode: FormattingMode) -> String {
    // Formatting is always ASCII, so we can just use slices
    // line
    match mode {
        FormattingMode::None => { line },
        FormattingMode::Text => { line },
        FormattingMode::Header1 => { line[1..].to_string() },
        FormattingMode::Header2 => { line[2..].to_string() },
        FormattingMode::Header3 => { line[3..].to_string() },
        FormattingMode::List => { line[2..].to_string() },
        FormattingMode::Quote => {line[1..].trim_start().to_string() },
        FormattingMode::Link => { line[2..].trim_start().to_string() },
        FormattingMode::Preformatted => { line[3..].to_string() },
    }
}

pub fn build_page(input: std::path::PathBuf, output: std::path::PathBuf) -> Result<()> {
    println!("Building: {} -> {}", input.display(), output.display());

    // Iterate over the file
    let input_file = File::open(input.clone())?;
    let reader = BufReader::new(input_file);

    let mut output_file = File::create(output)?;

    let mut previous_formatting = FormattingMode::None;
    let mut last_line_empty = true;
    let mut is_preformatted_mode = false;

    for line in reader.lines() {
        if !is_preformatted_mode {
            let safe_line = line.with_context(|| format!("could not parse file `{}`", input.display()))?;
            let current_formatting = formatting_mode_from_line(&safe_line);
            let line_without_formatting = line_without_formatting(safe_line, current_formatting);
            let is_line_empty = line_without_formatting.is_empty();

            // If formatting is the same, we don't have to change anything
            // TODO: For text, should we close current paragraph or just add a <br> ?
            if previous_formatting != current_formatting {
                if previous_formatting != FormattingMode::None {
                    writeln!(output_file, "{}", end_formatting(previous_formatting))?;
                }
                write!(output_file, "{}", start_formatting(current_formatting))?;
            } else {
                // We add a new space, but it is not allowed by the specification.
                // The advantage of doing so is that it's easier to wrap when screen
                // is too small (and we can use CSS to keep a fixed width).
                // In the future, a configuration option could be added to change
                // this behaviour.

                if is_line_empty {
                    write!(output_file, "<br>")?;
                } else if !last_line_empty {
                    write!(output_file, " ")?;
                }
            }

            if current_formatting == FormattingMode::List {
                writeln!(output_file, "<li>{}</li>", line_without_formatting)?;
            } else if current_formatting == FormattingMode::Link {
                // TODO: Rewrite using split_once()
                let mut splitter = line_without_formatting.split(' ');
                let url = splitter.next().unwrap();
                let mut description = splitter.fold("".to_string(), |a, b| a + " " + b);
                description = description.trim_start().to_string();
                if description.is_empty() {
                    description = url.to_string();
                }
                write!(output_file, "<a href='{}'>{}</a><br>", url, description)?;
            } else if current_formatting == FormattingMode::Preformatted {
                // TODO: Add support for alt text in preformatted mode
                is_preformatted_mode = true;
            } else {
                write!(output_file, "{}", line_without_formatting)?;
            }

            previous_formatting = current_formatting;
            last_line_empty = is_line_empty;
        } else {
            let current_formatting = formatting_mode_from_line(line.as_ref().unwrap());
            if current_formatting == FormattingMode::Preformatted {
                is_preformatted_mode = false;
            } else {
                writeln!(output_file, "{}", line.unwrap())?;
            }
        }
    }
    // At the end of the file, we need to close current formatting
    writeln!(output_file, "{}", end_formatting(previous_formatting))?;

    Ok(())
}

pub fn build(input: std::path::PathBuf, output: std::path::PathBuf) -> Result<()> {
    fs::create_dir_all(&output)
        .with_context(|| format!("could not create HTML output directory `{}`", output.display()))?;
    let files = fs::read_dir(&input)
        .with_context(|| format!("could not read directory `{}`", input.display()))?;
    for entry in files {
        let filename = entry.as_ref().unwrap().path().file_name().unwrap().to_str().unwrap().to_string();
        let path = entry.unwrap().path();
        if fs::metadata(path.clone()).with_context(|| format!("could not check if file is a directory `{}`",path.display()))?.is_dir() {
            match build(input.join(filename.clone()), output.join(filename)) {
                Ok(content) => { content },
                Err(error) => { return Err(error.into()); }
            }
        } else {
            let mut filename_html = filename.clone();
            // TODO: Optimize and make code more resilient
            for _iteration in 0..3 {
                filename_html.pop();
            }
            filename_html.push_str("html");
            match build_page(input.join(filename), output.join(filename_html)) {
                Ok(content) => { content },
                Err(error) => { return Err(error.into()); }
            }
        }
    }

    Ok(())
}