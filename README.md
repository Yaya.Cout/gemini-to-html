# Gemini To HTML Converter

This is a (WIP) project written in Rust to convert extended Gemini (with
include support, for example) to normal Gemini before converting it to HTML.

## Building

You can compile it as most Rust project : using `cargo build`
(`cargo build --release` to get an optimized version).

The binary can be found at `./target/debug/gmi-to-html` or
`./target/release/gmi-to-html`

## Run

If you want to build and run automatically after, just execute:

```shell
cargo run
# Or, to specify arguments
cargo run -- --input <input-dir> --output <output-dir>
```

## License

This software is released using GPL 3.0 or later license, to preserve user
freedom.

## Resources

- [Gemini Specification](https://geminiprotocol.net/docs/specification.gmi)
  (can also be viewed from Gemini)
